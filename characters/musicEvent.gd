extends Node2D

export(NodePath) var area_path
onready var trigger: Area2D = get_node(area_path)

func play(_body: Area2D):
	$AnimationPlayer.play("fade_in")

func _ready():
	trigger.connect("area_entered", self, "play")
