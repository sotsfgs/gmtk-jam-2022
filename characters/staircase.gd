extends Node2D

export(String, FILE, "*.tscn") var teleport_to

func _ready():
	$Area.connect("area_entered", self, "_when_area_enter")

func _when_area_enter(thing: Area2D):
	var thing_parent = thing.get_parent()
	if thing_parent.is_in_group("hero"):
		Teleporter.switch_to(teleport_to)
