extends Node2D

export(bool) var isLocked
export(String) var code

const doorOpener = "door_opener"

func _ready():
	$Origin/Nearby.connect("area_entered", self, "when_area_entered")
	$Origin/Nearby.connect("area_exited", self, "when_area_exited")

func switch_door_state(spriteClose: bool, spriteOpen: bool) -> void:
	$Origin/Close.visible = spriteClose
	$Origin/Open.visible = spriteOpen

func when_area_entered(thing: Area2D) -> void:
	var areaParent = thing.get_parent()
	if can_you_move(areaParent):
		door_open()
	else:
		$SFX/Locked.play()

func when_area_exited(_thing: Area2D) -> void:
	if not isLocked:
		door_close()

func door_open():
	$SFX/Open.play()
	switch_door_state(false, true)

func door_close():
	$SFX/Close.play()
	switch_door_state(true, false)

func can_you_move(who: Node2D) -> bool:
	if who.is_in_group("enemies"):
		return true
	if who.is_in_group("hero"):
		if isLocked:
			if Bag.key_open(code):
				isLocked = false

		if not isLocked:
			return true

	return false
