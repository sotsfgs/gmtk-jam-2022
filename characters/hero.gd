extends Node2D

onready var movement = $Movement

func _ready():
	movement.connect("movement_started", self, "play_walk")

func play_walk():
	$SFX/Walk.play()

func _input(event):
	for key in movement.inputs:
		if event.is_pressed() && event.is_action(key) || event.is_action_pressed(key):
			movement.perform_move(key)

func _process(_delta):
	set_process_input(not Teleporter.disable_input)
