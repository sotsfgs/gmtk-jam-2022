extends StaticBody2D

export(NodePath) var parent_path
onready var my_parent = get_node(parent_path)

func _ready():
	pass

func can_you_move(who: Node2D) -> bool:
	if my_parent.has_method("can_you_move"):
		return my_parent.can_you_move(who)
	
	return false
