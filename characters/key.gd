extends Node2D

export(String) var code
export(String) var unique_name
var picked = false

func bag_store_this():
	if not picked:
		picked = true

		Bag.store(unique_name, {
			"code": code,
			"type": "key"
		})
