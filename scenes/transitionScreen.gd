extends CanvasLayer

signal transitioned(goto)

var goto: String

func _ready():
	$AnimationPlayer.connect("animation_finished", self, "emit_when_finish")

func emit_when_finish(animation: String):
	if animation == "fade_out":
		emit_signal("transitioned", goto)
	elif animation == "fade_in":
		Teleporter.disable_input = false
		Bag.items = {}

func fade_in():
	$AnimationPlayer.play("fade_in")

func fade_out(here: String):
	$AnimationPlayer.play("fade_out")
	goto = here
