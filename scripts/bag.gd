extends Node

var items = {}

func key_open(door_code: String) -> bool:
	for item in items.keys():
		var thing = items[item]
		if thing["type"] == "key":
			if thing["code"] == door_code:
				return true

	return false

func store(name: String, content: Dictionary):
	items[name] = content
