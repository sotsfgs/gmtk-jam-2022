extends Node

var disable_input: bool

func _ready():
	TransitionScreen.connect("transitioned", self, "make_switch")


func make_switch(to: String):
	get_tree().change_scene(to)
	TransitionScreen.fade_in()

func switch_to(scene: String):
	disable_input = true
	TransitionScreen.fade_out(scene)
