extends Node2D

var tile_size = 16
export(int) var speed = 3
export(NodePath) var path
onready var Character: Node2D = get_node(path)
signal movement_started

var inputs = {
	"move_up": Vector2.UP,
	"move_down": Vector2.DOWN,
	"move_right": Vector2.RIGHT,
	"move_left": Vector2.LEFT
}

func _ready():
	Character.position = Character.position.snapped(Vector2.ONE * tile_size)
	Character.position += Vector2.ONE * tile_size/2

func perform_move(direction: String) -> void:
	if $Tween.is_active():
		return

	move(direction)

func move(to: String):
	var dir = inputs[to]

	$RayCast2D.cast_to = dir * tile_size
	$RayCast2D.force_raycast_update()

	if $RayCast2D.is_colliding():
		var collider = $RayCast2D.get_collider()
		if collider.has_method("can_you_move"):
			if collider.can_you_move(Character):
				move_tween(dir)
	else:
		move_tween(dir)

func move_tween(dir: Vector2):
	emit_signal("movement_started")
	$Tween.interpolate_property(Character, "position", Character.position, Character.position + dir * tile_size, 1.0/speed, Tween.TRANS_QUART, Tween.EASE_OUT)
	$Tween.start()
