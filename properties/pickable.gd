extends Node2D

export(NodePath) var queue_free_this_path
onready var queue_free_this = get_node(queue_free_this_path)

func _ready():
	$Area.connect("area_entered", self, "_when_area_entered")
	$Picked.connect("finished", self, "do_queue_free")

func do_queue_free():
	queue_free_this.queue_free()

func _when_area_entered(_area: Area2D):
	if queue_free_this.has_method("bag_store_this"):
		$Picked.play()
		queue_free_this.bag_store_this()
